def createStack():
    stack = []
    return stack

def push(stack,item):
    stack.append(item)

def size(stack):
    return len(stack)

def isEmpty(stack):
    if size(stack)==0:
        return True

def pop(stack):
    if isEmpty(stack):
        return
    return stack.pop()

def rev(string):
    n = len(string)
    stack = createStack()

    for i in range(n):
        push(stack,string[i])

    s = ''

    for i in range(n):
        x = pop(stack)
        s = s + x
    return s   


string = "RohitManjhi"
s = rev(string)
print("Reversed string is " + s)
