import re

# match same as full match
s = input()
t = input()
"""m = re.match(s,m)
if m!=None:
    print("Match available ",m.start(),  "or" , m.end())
else:
    print("Not available")
"""
# search
#m = re.search(t,s)
#if m!=None:
#    print("match is available", m.start(),m.end())

# findall
#l = re.findall("[0-9]", s)
#print(l)

# finditer
#itr = re.finditer("[-a-z]",s)
#for m in itr:
#    print(m.start(), m.end(), m.group())

# sub
#l = re.sub("[aeiouAEIOU]","$",s)
#x = re.sub("[^aeiouAEIOU]","@",s)
#print(l)
#print(x)
#print(s.upper())

# subn
#l = re.subn("[a-z]","$",s)
#print(l)
#print(l[0])
#print(l[1])

# split
#l = re.split(' ',s)
#print(l)

# symbol ^ or $ OR RE.ignorecase
"""chk = re.search("^rohit",s,re.IGNORECASE)
if chk != None:
    print("start with rohit")
last = re.search("manjhi$", s,re.IGNORECASE)
if last != None:
    print('Ends with manjhi')
"""

# fullmatch
"""m = re.fullmatch("[a-k][0369][a-zA-Z0-9#]*",s)
if m!=None:
    print('Yupp')
else:
    print("Fail")
"""
"""# mobile number
m = re.fullmatch("[7-9][0-9]{9}",s)
if m!=None:
    print('Yupp')
else:
    print("Fail")
 """
# mailId
m = re.fullmatch("\w[a-zA-Z0-9_.]*@gmail[.]com",s)
if m!=None:
    print('Yupp')
else:
    print("Fail")

# digit
m = re.fullmatch("-?[0-9]*",s)
if m!=None:
    print('Yupp',m)
else:
    print("Fail")


